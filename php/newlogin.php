<?php
    include_once '../modelo/database.php';

    session_start();

    if(isset($_GET['cerrar_sesión'])){
        session_unset();

        session_destroy();
    }

    if(isset($_SESSION['rol'])){
        switch($_SESSION['rol']){
            case 1:
                header('location: ../pages/miCuentaAdmin.php');
                break;
            case 2:
                header('location: ../pages/miCuenta.php');
                break;
            default:
        }
    }

    if(isset($_REQUEST['email']) && isset($_REQUEST['password'])){
        $email = $_REQUEST['email'];
        $password = $_REQUEST['password'];

        $db = new database();
        $query = $db->conectar()->prepare('SELECT * FROM users WHERE email = :email AND password = :password');
        $query->execute(['email'=>$email, 'password' =>$password]);

        $row=$query->fetch(PDO::FETCH_NUM);
        if($row==true){
            //validar rol
            $rol = $row[6];
            $_SESSION['rol'] = $rol;
            switch($_SESSION['rol']){
                case 1:
                    header('location: ../pages/miCuentaAdmin.php');
                    break;
                case 2:
                    header('location: ../pages/miCuenta.php');
                    break;
                default:
            }
        }else{
            //no exsiste el usuario
            /* echo "El usuario o contraseña no son correctos"; */
            header('Location: ../pages/iniciarSesion.php?failed_login=true');
        }
    }

?>