<?php

        require "../modelo/products.php";
        require '../modelo/config.php';

        $products=array();
        $productModel = new ProductModel();
        $products=$productModel->getAllProducts();
        header("Content-Type: application/json");
        echo json_encode($products);
    ?>