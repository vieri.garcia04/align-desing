<?php

        require "../modelo/users.php";

        $name = $_REQUEST['nombre'];
        $lastname = $_REQUEST['apellidos'];
        $email = $_REQUEST['email'];
        $password = $_REQUEST['password'];
        $birthday = $_REQUEST['nacimiento'];
        $privacy_terms = $_REQUEST['privacidad']|| FALSE;
        $condition_terms = $_REQUEST['condiciones'] || FALSE;

        $failed_created_account=FALSE;

    
        try {
            $userModel = new UserModel();
            $res=$userModel->createUser($name, $lastname, $email, $password, $birthday);
            if($res==FALSE){
                $failed_created_account=TRUE;
            }
        } catch (Exception $e) {
            $failed_created_account=TRUE;
        }
       
        if ($failed_created_account){
            header('Location: ../pages/crearCuenta.php?failed_created_account=true');
        }else{
            header('Location: ../pages/iniciarSesion.php');
        }
    ?>