-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-06-2022 a las 04:51:31
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `test`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compra`
--

CREATE TABLE `compra` (
  `id` int(11) NOT NULL,
  `id_transaccion` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` datetime NOT NULL,
  `status` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `id_cliente` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `total` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `compra`
--

INSERT INTO `compra` (`id`, `id_transaccion`, `fecha`, `status`, `email`, `id_cliente`, `total`) VALUES
(1, '21N25954PC856930S', '2022-06-19 04:20:12', 'COMPLETED', 'sb-dyrv814657275@personal.example.com', 'B5MS4SB7BA4E2', '548'),
(2, '8LF56816TS1736007', '2022-06-19 05:39:48', 'COMPLETED', 'sb-dyrv814657275@personal.example.com', 'B5MS4SB7BA4E2', '286'),
(3, '97P64255DF541803D', '2022-06-19 05:49:36', 'COMPLETED', 'sb-dyrv814657275@personal.example.com', 'B5MS4SB7BA4E2', '286'),
(4, '8E505755LB2109538', '2022-06-19 07:46:00', 'COMPLETED', 'sb-dyrv814657275@personal.example.com', 'B5MS4SB7BA4E2', '286'),
(5, '71352640FC504012X', '2022-06-20 03:24:49', 'COMPLETED', 'sb-dyrv814657275@personal.example.com', 'B5MS4SB7BA4E2', '1585'),
(6, '81C75965U0123443K', '2022-06-20 04:04:45', 'COMPLETED', 'sb-dyrv814657275@personal.example.com', 'B5MS4SB7BA4E2', '1418'),
(7, '83X10533H54734300', '2022-06-20 04:10:01', 'COMPLETED', 'sb-dyrv814657275@personal.example.com', 'B5MS4SB7BA4E2', '1349');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_compra`
--

CREATE TABLE `detalle_compra` (
  `id` int(11) NOT NULL,
  `id_compra` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `nombre` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `precio` decimal(10,0) NOT NULL,
  `cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `detalle_compra`
--

INSERT INTO `detalle_compra` (`id`, `id_compra`, `id_producto`, `nombre`, `precio`, `cantidad`) VALUES
(1, 1, 6, 'MOCHILA ADICOLOR BACKPACK NAVY', '119', 1),
(2, 1, 4, 'ADIDAS POLO SST SS TEE', '249', 1),
(3, 1, 8, 'TRACKPANT RED', '90', 2),
(4, 2, 7, 'CL Summer Retreat SACK', '127', 1),
(5, 2, 12, '3-STRIPE SHORT', '159', 1),
(6, 3, 7, 'CL Summer Retreat SACK', '127', 1),
(7, 3, 12, '3-STRIPE SHORT', '159', 1),
(8, 4, 7, 'CL Summer Retreat SACK', '127', 1),
(9, 4, 12, '3-STRIPE SHORT', '159', 1),
(10, 5, 2, 'CLUB C 85 WHITE', '349', 2),
(11, 5, 1, 'LEGACY CANVAWMNS COURTS', '224', 2),
(12, 5, 3, 'RS-Z', '439', 1),
(13, 6, 3, 'RS-Z', '439', 1),
(14, 6, 1, 'LEGACY CANVAWMNS COURTS', '224', 3),
(15, 6, 5, 'CLASSIC LOGO AOP TEE', '90', 2),
(16, 6, 7, 'CL Summer Retreat SACK', '127', 1),
(17, 7, 7, 'CL Summer Retreat SACK', '127', 10),
(18, 7, 10, '3STR SHORTS', '79', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `marca` text COLLATE utf8_spanish_ci NOT NULL,
  `precio` decimal(10,0) NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `descuento` tinyint(3) NOT NULL DEFAULT 0,
  `id_categoria` int(11) NOT NULL,
  `activo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `nombre`, `marca`, `precio`, `descripcion`, `descuento`, `id_categoria`, `activo`) VALUES
(1, 'LEGACY CANVAWMNS COURTS', 'NIKE', '249', '', 10, 1, 1),
(2, 'CLUB C 85 WHITE', 'REEBOK', '349', '', 0, 1, 1),
(3, 'RS-Z', 'PUMA', '439', '', 0, 1, 1),
(4, 'ADIDAS POLO SST SS TEE', 'ADIDAS', '249', '', 0, 1, 1),
(5, 'CLASSIC LOGO AOP TEE', 'CHAMPION', '90', '', 0, 1, 1),
(6, 'MOCHILA ADICOLOR BACKPACK NAVY', 'ADIDAS', '119', '', 0, 1, 1),
(7, 'CL Summer Retreat SACK', 'REEBOOK', '127', '', 0, 1, 1),
(8, 'TRACKPANT RED', 'ADIDAS', '90', '', 0, 1, 1),
(9, 'TRACKPANT', 'ADIDAS', '131', '', 0, 1, 1),
(10, '3STR SHORTS', 'ADIDAS', '79', '', 0, 1, 1),
(11, 'PANTS', 'ADIDAS', '229', '', 0, 1, 1),
(12, '3-STRIPE SHORT', 'ADIDAS', '159', '', 0, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `name` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `lastname` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `password` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `birthday` date NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`name`, `lastname`, `email`, `password`, `birthday`, `id`) VALUES
('augusto', 'barrientos mejia', 'u20204717@utp.edu.pe', '12345', '1998-08-18', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `compra`
--
ALTER TABLE `compra`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `detalle_compra`
--
ALTER TABLE `detalle_compra`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `compra`
--
ALTER TABLE `compra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `detalle_compra`
--
ALTER TABLE `detalle_compra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
