<?php
    require 'modelo/config.php';
    require 'modelo/database.php';
    $db = new Database();
    $con = $db->conectar();

    $sql = $con->prepare("SELECT id, nombre, marca, precio FROM productos WHERE activo=1");
    $sql->execute();
    $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);

    // session_destroy();
    print_r($_SESSION);

?>



<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">

    
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;400&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Anton&family=Kanit:wght@100;400&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cinzel:wght@700;800&family=Montserrat&display=swap" rel="stylesheet">

    <script rel="stylesheet" src="https://kit.fontawesome.com/c174601175.js" crossorigin="anonymous"></script>

    <meta http-equiv=”Expires” content=”0″>
    <meta http-equiv=”Last-Modified” content=”0″>
    <meta http-equiv=”Cache-Control” content=”no-cache, mustrevalidate”>
    <meta http-equiv=”Pragma” content=”no-cache”>
    
    <link rel="stylesheet" href="./css/header.css">
    <link rel="stylesheet" type="text/css" href="./css/header.css">
    <link rel="stylesheet" href="./css/footer.css">
    <link rel="stylesheet" type="text/css" href="./css/footer.css">
    <link rel="stylesheet" href="./css/index.css">
    <link rel="stylesheet" type="text/css" href="./css/index.css">
    <link rel="stylesheet" href="./css/FontAwesome/all.min.css">
    <link rel="icon" type="image/png" href="./img/logo-alignStyle.png">

    <title>INICIO | ALIGN STYLE</title>
</head>

<body>
    <!-- ********** MENÚ ********** -->
    <header>
        <div class="descuento">
            <h6>ENVIOS GRATUITOS POR COMPRAS MAYORES A S/ 299 SOLES</h6>
        </div>
        <nav class="contenedor_menu">
            <input type="checkbox" id="check">
            <label for="check" class="checkbtn"><i class="fa-solid fa-bars"></i></label>
            <a class="titulo" href="./index.php" target="_top">ALIGN STYLE</a>

            <ul class="ul-header">
                <li><a class="texto" href="./pages/zapatillas.html" target="_top">ZAPATILLAS <i
                            class="fa-solid fa-chevron-down"></i></a></li>
                <li><a class="texto" href="./pages/ropa.php" target="_top">ROPA <i
                            class="fa-solid fa-chevron-down"></i></a></li>
                <li><a class="texto" href="./pages/accesorios.html" target="_top">ACCESORIOS <i
                            class="fa-solid fa-chevron-down"></i></a></li>
                <li><a class="texto" href="./pages/marcas.html" target="_top">MARCAS <i
                            class="fa-solid fa-chevron-down"></i></a>
                    <ul class="sub_menu">
                        <li><a href="./pages/adidas.html"><img class="img_prod" src="./img/marcas/adidas.jpeg"
                                    alt=""></a></li>
                        <li><a href=""><img class="img_prod" src="./img/marcas/nb.jpeg" alt=""></a></li>
                        <li><a href=""><img class="img_prod" src="./img/marcas/puma.jpeg" alt=""></a></li>
                        <li><a href=""><img class="img_prod" src="./img/marcas/nike.jpeg" alt=""></a></li>
                        <li><a href=""><img class="img_prod" src="./img/marcas/converse.jpeg" alt=""></a></li>
                        <li><a href=""><img class="img_prod" src="./img/marcas/champion.jpeg" alt=""></a></li>
                        <li><a href=""><img class="img_prod" src="./img/marcas/crocs.jpeg" alt=""></a></li>
                        <li><a href=""><img class="img_prod" src="./img/marcas/kappa.jpeg" alt=""></a></li>
                        <li><a href=""><img class="img_prod" src="./img/marcas/ne.jpeg" alt=""></a></li>
                        <li><a href=""><img class="img_prod" src="./img/marcas/rebels.jpeg" alt=""></a></li>
                    </ul>
                </li>
            </ul>

            <div>

                <i id="search" class="fa-solid fa-magnifying-glass logos buscar" target="_top"></i> <!-- Busqueda-->
                <a class="logos login" href="./pages/miCuenta.php" target="_top"><i
                        class="fa-solid fa-user"></i></a>
                <!--Inicio Sesión -->
                <a class="logos bolsa" href="checkout.php" target="_top"><i
                        class="fa-solid fa-bag-shopping"></i><span id="num_cart" class="badge bg-secondary contador"><?php echo $num_cart; ?></span></a>
                <!--Carrito-->
            </div>
        </nav>

        <!-- ********************************  BUSCADOR  *******************************+ -->
        <div class="ctn-bars-search" id="ctn-bars-search" action="">
            <input class="buscar-input" id="input-search" type="text" placeholder="Buscar">
        </div>

        <ul id="box-search">
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Polo</a></li>
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Zapatilla</a></li>
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Pantalon</a></li>
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Camisa</a></li>
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Gorro</a></li>
        </ul>

        <div id="cover-ctn-search"></div>
        <!-- ***************************************************************************** -->
    </header>


    <!-- ********** CUERPO DE LA PÁGINA ********** -->

    <div class="container-slider">
        <div class="slider" id="slider">
            <div class="slider__section">
                <img src="./img/home/img_principal.jpg" alt="" class="slider_img">
            </div>
            <div class="slider__section">
                <img src="./img/home/imgprincipal2.jpg" alt="" class="slider_img">
            </div>
            <div class="slider__section">
                <img src="./img/home/imgprincipal3.jpg" alt="" class="slider_img">
            </div>
            <div class="slider__section">
                <img src="./img/home/imgprincipal4.jpeg" alt="" class="slider_img">
            </div>
        </div>
        <div class="slider__btn slider__btn--right" id="btn-right">&#62;</div>
        <div class="slider__btn slider__btn--left" id="btn-left">&#60;</div>
    </div>
    <script src="./js/slider.js"></script>

    <main>
        <div class="container">
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
                <?php
                    foreach ($resultado as $row){ ?>
                        <div class="col">
                            <div class="card shadow-sm">
                                <?php
                                    $id = $row['id'];
                                    $imagen="images/productos/" .$id. "/principal.jpeg";

                                    if(!file_exists($imagen)){
                                        $imagen = "images/no-photo.jpg";
                                    }

                                ?>


                                <img src="<?php echo $imagen; ?>" alt="">
                                <div class="card-body">
                                    <h5 class="card-title"><?php echo $row['marca']; ?></h5>
                                    <p class="card-text"><?php echo $row['nombre']; ?></p>
                                    <p class="card-text">S/. <?php echo number_format($row['precio'], 2, '.', ',') ; ?></p>
                                    <div class="d-flex justify-content-between align-items-center">

                                        <div class="btn-group">
                                            <a href="detalles.php?id=<?php echo $row['id']; ?>&token=<?php echo hash_hmac('sha1', $row['id'], KEY_TOKEN); ?>" class="btn btn-primary">Detalles</a>
                                        </div>
                                        <button class="btn btn-outline-success" type="button" onclick="addProducto(<?php  echo $row['id']; ?>, '<?php  echo hash_hmac('sha1', $row['id'], KEY_TOKEN); ?>')">Agregar al carrito</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } 
                ?>  
            </div>
        </div>
    </main>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>

    <script>
        function addProducto(id, token){
            let url = 'controlador/carrito.php'
            let formData = new FormData()
            formData.append('id', id)
            formData.append('token', token)

            fetch(url, {
                method: 'POST',
                body: formData,
                mode: 'cors'
            }).then(response => response.json())
            .then(data => {
                if(data.ok){
                    let elemento = document.getElementById("num_cart")
                    elemento.innerHTML = data.numero
                }
            })  
        }
    </script>


    <!-- ********** FOOTER ********** -->

    <footer>
        <div class="datosencabezado"> <!-- Encabezado de footer -->
            <div class="seccionencabezado">
                <a class="titulo" href="./index.php" target="_top">ALIGN STYLE</a> <!-- ALIGN STYLE -->
            </div>
            <div class="seccionencabezado">
                <a class="subtitulo">TIENDA TRUJILLO</a>
            </div>
            <div class="seccionencabezado">
                <a class="subtitulo">TIENDA CHICLAYO</a>
            </div>
            <div class="seccionencabezado">
                <a class="subtitulo">NUESTRA EMPRESA</a>
            </div>
        </div>
        <div class="datos"> <!-- datos de footer -->
            <div class="seccion">
                <div class="secciondatos">
                    <a class="logodatos"><i class="fa-solid fa-location-dot"></i></a> <!-- icono gps -->
                    <div class="texto">
                        Primer Nivel <br>
                        Barrio Jocker Plaza <br>
                        Encuéntranos frente a <br>
                        Calvin Klein
                    </div>
                </div> <br>
                <div class="secciondatos">
                    <a class="logodatos"><i class="fa-solid fa-clock"></i></a><!-- Icono de reloj -->
                    <div class="texto">
                        Horario de atención: <br>
                        10:00 am - 10:00 pm <br>
                        Perú
                    </div>
                </div> <br>
                <div class="secciondatos">
                    <a class="logodatos"><i class="fa-solid fa-phone"></i></a> <!-- Icono de telefono -->
                    <div class="texto">
                        966 677 227 <br>
                        Atención al cliente
                    </div>
                </div> <br>
                <div class="secciondatos">
                    <a class="logodatos"><i class="fa-solid fa-envelope"></i></a><!-- Icono de mail -->
                    <div class="texto">
                        soporte@alignstyle.pe
                    </div>
                </div>
            </div>
            <div class="seccion">
                <div class="secciondatos">
                    <a class="logodatos"><i class="fa-solid fa-location-dot"></i></a> <!-- icono gps -->
                    <div class="texto">
                        Av. Húsares de junín 316 <br>
                        La Merced
                    </div>
                </div> <br>
                <div class="secciondatos">
                    <a class="logodatos"><i class="fa-solid fa-clock"></i></a><!-- Icono de reloj -->
                    <div class="texto">
                        Horario de atención: <br>
                        Lunes a Domingo <br>
                        10:00 am - 9:00 pm <br>
                        Perú
                    </div>
                </div>
            </div>
            <div class="seccion">
                <div class="secciondatos">
                    <a class="logodatos"><i class="fa-solid fa-location-dot"></i></a> <!-- icono gps -->
                    <div class="texto">
                        Calle Alfonso Ugarte 850
                    </div>
                </div> <br>
                <div class="secciondatos">
                    <a class="logodatos"><i class="fa-solid fa-clock"></i></a><!-- Icono de reloj -->
                    <div class="texto">
                        Horario de atención: <br>
                        Lunes a Sábado <br>
                        10:00 am - 09:00 pm <br>
                        Domingos <br>
                        11:00 am - 7:00 pm <br>
                        Perú
                    </div>
                </div>
            </div>
            <div class="seccion">
                <div class="secciondatos">
                    <a class="logodatos"></a>
                    <div class="texto">
                        <a class="texto">Política de privacidad</a><br> <!-- Target Top para que se cargue la pagina otra vez y no se cree un cuadro con la otra ventana -->
                        <a class="texto">Términos y condiciones</a> <br>
                        <a class="texto">Tiendas</a><br>
                        <a class="texto" href="./pages/nosotros.html" target="_top">Nosotros</a><br>
                        <a class="texto" href="./pages/equipo.html" target="_top">Equipo</a><br>
                        <a class="texto" href="./pages/contactanos.html" target="_top">Contáctanos</a><br>
                        <a class="texto" href="./pages/servicios.html" target="_top">Servicios</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright">
            <div class="copy">
                ALIGN STYLE &copy; 2022, ALL RIGHT RESERVERED
            </div>
            <div class="info">
                Mas información: 
                <a class="logos-a" href="https://www.facebook.com/ALIGN-STYLE-100589032692689" target="_blank"><i class="fa-brands fa-facebook"></i></a>
                <a class="logos-a" href="https://www.instagram.com/align.style/" target="_blank"><i class="fa-brands fa-instagram"></i></a>
                <a class="logos-a" href="https://wa.me/message/4W4MPALEQJZGE1" target="_blank"><i class="fa-brands fa-whatsapp"></i></a>
                <a class="logos-a" href="https://vm.tiktok.com/ZMNRk35WR/" target="_blank"><i class="fa-brands fa-tiktok"></i></a>
            </div>
        </div>
    </footer>

    <script src="./js/header.js"></script>
    <script src="./js/carrito.js"></script>
    <script src="./js/pedido.js"></script>
    <script src="https://account.snatchbot.me/script.js"></script><script>window.sntchChat.Init(252214)</script> 
</body>

</html>