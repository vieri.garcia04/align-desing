<?php

    require 'modelo/config.php';
    require 'modelo/database.php';
    $db = new Database();
    $con = $db->conectar();

    $id = isset($_GET['id']) ? $_GET['id'] : '';
    $token = isset($_GET['token']) ? $_GET['token'] : '';
    echo $token;
    if ($id == '' || $token == ''){
        echo 'Error al procesar la peticion';
        exit;
    }else{

        $token_tmp = hash_hmac('sha1', $id, KEY_TOKEN);

        if($token == $token_tmp){
            $sql = $con->prepare("SELECT count(id) FROM productos WHERE id=? AND activo=1");
            $sql->execute([$id]);

            if($sql->fetchColumn() > 0){
                $sql = $con->prepare("SELECT nombre, marca, precio, descripcion, descuento FROM productos WHERE id=? AND activo=1 LIMIT 1");
                $sql->execute([$id]);
                $row = $sql->fetch(PDO::FETCH_ASSOC);
                $nombre = $row['nombre'];
                $marca = $row['marca'];
                $precio = $row['precio'];
                $descripcion = $row['descripcion'];
                $descuento = $row['descuento'];
                $precio_desc = $precio - (($precio * $descuento) / 100);
                $dir_images = 'images/productos/' . $id . '/';

                $rutaImg = $dir_images . 'principal.jpeg';

                if(!file_exists($rutaImg)){
                    $rutaImg = 'images/no-photo.jpg';
                }
                $imagenes = array();

                if(file_exists($dir_images)){
                    $dir = dir($dir_images);
                    while(($archivo = $dir->read()) != false){
                        if($archivo != 'principal.jpeg' && (strpos($archivo, 'jpg') || strpos($archivo, 'jpeg'))){
                            $imagenes[] = $dir_images . $archivo;
                        }
                    }
                    $dir->close();
                }
            }else{
            echo 'Error al procesar la peticion';
            exit;
        }
    }
}

            // $sql = $con->prepare("SELECT id, nombre, marca, precio FROM productos WHERE activo=1");
            // $sql->execute();
            // $resultado = $sql->fetchAll(PDO::FETCH_ASSOC);
            
    
?>



<!DOCTYPE html>
<html lang="es">
<>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <script rel="stylesheet" src="https://kit.fontawesome.com/c174601175.js" crossorigin="anonymous"></script>
    <link href="css/estilos.css" rel="stylesheet">

    <link rel="stylesheet" href="./css/header.css">
    <link rel="stylesheet" type="text/css" href="./css/header.css">
    <link rel="stylesheet" href="./css/footer.css">
    <link rel="stylesheet" type="text/css" href="./css/footer.css">
    <link rel="stylesheet" href="./css/detalles.css">
    <link rel="stylesheet" type="text/css" href="./css/detalles.css">
    <link rel="stylesheet" href="./css/FontAwesome/all.min.css">
    <link rel="icon" type="image/png" href="./img/logo-alignStyle.png">
  
    <title>Tienda</title>
</head>
<body>
    <header>
        <div class="descuento">
            <h6>ENVIOS GRATUITOS POR COMPRAS MAYORES A S/ 299 SOLES</h6>
        </div>
        <nav class="contenedor_menu">
            <input type="checkbox" id="check">
            <label for="check" class="checkbtn"><i class="fa-solid fa-bars"></i></label>
            <a class="titulo" href="./index.php" target="_top">ALIGN STYLE</a>

            <ul class="ul-header">
                <li><a class="texto" href="./pages/zapatillas.html" target="_top">ZAPATILLAS <i class="fa-solid fa-chevron-down"></i></a></li>
                <li><a class="texto" href="./pages/ropa.php" target="_top">ROPA <i class="fa-solid fa-chevron-down"></i></a></li>
                <li><a class="texto" href="./pages/accesorios.html" target="_top">ACCESORIOS <i class="fa-solid fa-chevron-down"></i></a></li>
                <li><a class="texto" href="./pages/marcas.html" target="_top">MARCAS <i class="fa-solid fa-chevron-down"></i></a></li>
            </ul>
        
            <div>
                
                <i id="search" class="fa-solid fa-magnifying-glass logos buscar" target="_top"></i>    <!-- Busqueda-->
                <a class="logos login" href="./pages/miCuenta.php" target="_top"><i class="fa-solid fa-user"></i></a> <!--Inicio Sesión -->
                <a class="logos bolsa" href="./checkout.php" target="_top"><i class="fa-solid fa-bag-shopping"></i><span id="num_cart" class="badge bg-secondary contador"><?php echo $num_cart; ?></span></a><!--Carrito-->
            </div>
        </nav>

        <!-- ********************************  BUSCADOR  *******************************+ -->
        <div class="ctn-bars-search" id="ctn-bars-search" action="">
            <input class="buscar-input" id="input-search" type="text" placeholder="Buscar">
        </div>

        <ul id="box-search">
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Polo</a></li>
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Zapatilla</a></li>
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Pantalon</a></li>
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Camisa</a></li>
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Gorro</a></li>
        </ul>

        <div id="cover-ctn-search"></div>
        <!-- ***************************************************************************** -->
    </header>

    <main>
        <div class="container">
            <div class="row">
                <div class="col-md-6 order-md-1">
                    <img src="<?php echo $rutaImg;?>" alt="" class="d-block w-100">
                </div>
                <div class="col-md-6 order-md-2 informacion_detalle">
                    <h2><?php echo $nombre; ?></h2>

                    <?php if($descuento > 0) { ?>
                        <p><del><?php echo MONEDA . number_format($precio, 2, '.', ','); ?></del></p>
                        <h2>
                            <?php echo MONEDA . number_format($precio_desc, 2, '.', ','); ?>
                            <small class="text-success"><?php echo $descuento; ?>% descuento</small>
                        </h2>
                        <?php }else{ ?>
                            <h2><?php echo MONEDA . number_format($precio, 2, '.', ','); ?></h2>
                        <?php } ?>


                    <p class="lead">
                        <?php echo $marca; ?>
                    </p>

                    <div class="d-grid gap-3 col-10 mx-auto">
                        <button class="btn btn-primary" type="button">Comprar ahora</button>
                        <button class="btn btn-outline-primary" type="button" onclick="addProducto(<?php echo $id; ?>, '<?php echo $token_tmp; ?>')">Agregar al carrito</button>

                    </div>
                </div>
            </div>
        </div>
    </main>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>

    <script>
        function addProducto(id, token){
            let url = 'controlador/carrito.php'
            let formData = new FormData()
            formData.append('id', id)
            formData.append('token', token)

            fetch(url, {
                method: 'POST',
                body: formData,
                mode: 'cors'
            }).then(response => response.json())
            .then(data => {
                if(data.ok){
                    let elemento = document.getElementById("num_cart")
                    elemento.innerHTML = data.numero
                }
            })

        }
    </script>
        <script src="https://account.snatchbot.me/script.js"></script><script>window.sntchChat.Init(252214)</script> 

</body>
</html>