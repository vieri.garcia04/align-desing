
const nombre = document.getElementById("nombre");
const apellido = document.getElementById("apellido");
const email = document.getElementById("correo");



const form = document.getElementById("formDatosPersonales");


form.addEventListener("submit", e=>{

    e.preventDefault()
    let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;


    if(nombre.value.length <= 2){
        alert("Nombre muy corto");
    }
    else if(nombre.value.length > 30){
        alert("Nombre muy largo");
    }

    if(apellido.value.length < 2){
        alert("Apellido muy corto");
    }else if(apellido.value.length > 30){
        alert("Apellido muy largo");
    }

    if(regexEmail.test(email.value)){
        alert("El email no es valido");
    }
});