
producto = [
    { 'id': 11, 'marca': 'ADIDAS', 'nombre': 'HER COURT W', 'precio': 229,
     'imagen': '../img/zapatillas/her-court-w.webp','tipo':'TP01','sexo':'F'},
    { 'id': 12, 'marca': 'ADIDAS', 'nombre': 'TOP TEN HI', 'precio': 249,
     'imagen': '../img/zapatillas/top-ten-hi.webp','tipo':'TP01','sexo':'M'},
    { 'id': 13, 'marca': 'ADIDAS', 'nombre': 'FORUM MID', 'precio': 229,
     'imagen': '../img/zapatillas/forum-mid.webp','tipo':'TP01','sexo':'M'},
    { 'id': 14, 'marca': 'NEW BALANCE', 'nombre': 'NEW BALANCE 327', 'precio': 229,
     'imagen': '../img/zapatillas/new-balance-327.webp','tipo':'TP01','sexo':'M'},
    { 'id': 15, 'marca': 'NEW BALANCE', 'nombre': 'NEW BALANCE 574', 'precio': 229,
     'imagen': '../img/zapatillas/new-balance-574.webp','tipo':'TP01','sexo':'U'},
    { 'id': 16, 'marca': 'NEW BALANCE', 'nombre': 'NEW BALANCE 997', 'precio': 229,
     'imagen': '../img/zapatillas/new-balance-997.webp','tipo':'TP01','sexo':'U'},
    { 'id': 17, 'marca': 'CROCS', 'nombre': 'CROCS CLASSIC CLOG MEN', 'precio': 129,
     'imagen': '../img/zapatillas/crocs-classic-clog-men.webp','tipo':'TP01','sexo':'M'}, 
    { 'id': 18, 'marca': 'CROCS', 'nombre': 'CROCS CLASSIC CLOG WOMEN', 'precio': 129,
     'imagen': '../img/zapatillas/crocs-classic-clog-women.WEBP','tipo':'TP03','sexo':'F'},
    { 'id': 19, 'marca': 'CONVERSE', 'nombre': 'CT AS CX HI', 'precio': 249,
     'imagen': '../img/zapatillas/ct-as-cx-hi.WEBP','tipo':'TP01','sexo':'F'}

]

marca=[
    {'id':'ADIDAS','nombre':'ADIDAS'},
    {'id':'CROCS','nombre':'CROCS'},
    {'id':'NEW BALANCE','nombre':'NEW BALANCE'},
    {'id':'NIKE','nombre':'NIKE'},
    {'id':'PUMA','nombre':'PUMA'},
    {'id':'REEBOK','nombre':'REEBOK'},
    {'id':'VANS','nombre':'VANS'},
]

sexo=[
    {'id':'M','nombre':'Hombre'},
    {'id':'F','nombre':'Mujer'},
    {'id':'U','nombre':'Unisex'},
]


products_container = document.getElementById("products-container");
products_title = document.getElementById("products-title");
filtro_marca = document.getElementById("filtro-marcas");
filtro_sexo =document.getElementById("filtro-sexo");
let breadcrum = document.getElementById("breadcrumb");
let breadcrum_niveles=["Inicio","Zapatillas"];
let popover_is_showed=false;
let titulo="ZAPATILLAS";

function show_breadcrum(){
    breadcrum.innerHTML="";
    let i=1;
    let body="";
    breadcrum_niveles.forEach(nivel=>{
        let tag ="";
        if (i==breadcrum_niveles.length){
            tag='<li>'+nivel+'</li>'
        }else{
            tag='<li><a href="#">'+nivel+'</a></li>' 
        }
        body+=tag;
        i++;
    })
    breadcrum.innerHTML=body;

}

function product_to_tag(product) {
    tag_text = '<div class="contenedor_un_producto" href="../pages/producto.html">' +
        '<img class="imagen_prods" src="'+ product.imagen+'">' +
        '<br><br>'+
    '<div class="text_prods">' +
        '<h3 >'+product.marca+'</h3>' +
        '<p >'+product.nombre+'<br>' +
        'S/.'+parseFloat(product.precio).toFixed(2)+'</p>'+
    '</div>'+
    '</div>'
    tag=document.createElement("div");
    tag.innerHTML=tag_text;
    return tag
}

function set_products_section_title(title, filter_products){
    products_title.innerHTML="";
    tag=document.createElement("div");
    tag.style.display='flex';
    total_products= filter_products.length;
    let x=" productos"
    if ( total_products==1){
        x=" producto"
    }
    tag.innerHTML="<h2>"+title+"</h2><h4 class=\"subtitulo_productos\">"+total_products+""+x+" </h4>";
    products_title.appendChild(tag)
}

function set_filters(filters, filter_tag, filter_type){
    filters.forEach(filter => {
        tag=document.createElement("div");
        tag.className='filtro'
        tag.innerHTML='<label><input type="checkbox" name="'+filter_type+'" value="'+filter.id+'">'+ filter.nombre+'</label><br>'
        filter_tag.appendChild(tag);
    });
}

function show_filter_products(){
    productos_filtrados.forEach(producto => {
        products_container.appendChild(product_to_tag(producto));
    });
}

function capital_letter(text){
    let first_letter=text.substring(0,1);
    let incomplete_text = text.substring(1);
    return first_letter.toUpperCase()+incomplete_text;
}

// INICIALIZADOR
show_breadcrum()
set_filters(marcas,filtro_marcas,'marca')
set_filters(sexos,filtro_sexos,'sexo')
productos_filtrados_por_tipo=productos
productos_filtrados=productos_filtrados_por_tipo

set_products_section_title(titulo,productos_filtrados)

show_filter_products();


// ESCUCHADORES DE EVENTO
var checkboxes = document.querySelectorAll("input[type=checkbox]");


// Use Array.forEach to add an event listener to each checkbox.
checkboxes.forEach(function(checkbox) {
  checkbox.addEventListener('change', function() {

    let filter_type=this.name
    let filter_name=this.value
    if(this.checked){

        productos_filtrados=productos_filtrados_por_tipo.filter(producto => producto[filter_type] == filter_name );
        checkboxes.forEach(checkbox=>{
            if(checkbox.value!=filter_name){
                checkbox.checked=false;
            }
        })
    }else{
        productos_filtrados=productos
    }
    products_container.innerHTML="";
    set_products_section_title(titulo,productos_filtrados)
    show_filter_products()
  })
});

zapatillas_boton.addEventListener('click',e=>{
    show_popover(e.x, e.y+10)
})

var popover_options = document.querySelectorAll("li[class=popover-option]");
popover_options.forEach(popover =>{
    popover.addEventListener('click', e=>{
        let product_type=e.srcElement.id;
        let new_title=e.srcElement.innerHTML;
        breadcrum_niveles=["Inicio","Zapatillas"]
        if (product_type!="ALL"){
            productos_filtrados_por_tipo=productos.filter(producto => producto.tipo == product_type );
            titulo=new_title;
            breadcrum_niveles.push(capital_letter(new_title.toLowerCase()))
        }else{
            productos_filtrados_por_tipo=productos;
            titulo="Zapatillas"    
        }
        productos_filtrados=productos_filtrados_por_tipo
        products_container.innerHTML="";
        show_breadcrum()
        set_products_section_title(titulo,productos_filtrados)
        show_filter_products()
    })
})