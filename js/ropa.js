

// Funciones
async function initPage() {
    breadcrum_niveles = ["Inicio", "Ropa"]
    popover_is_showed = false;
    titulo = "ROPA"
    productos = await getProducts();
    marcas = getBrands(productos)
    tallas = [
        { 'id': 'S', 'nombre': 'S' },
        { 'id': 'M', 'nombre': 'M' },
        { 'id': 'L', 'nombre': 'L' },
        { 'id': 'XL', 'nombre': 'XL' }
    ]
    sexos = [
        { 'id': 'M', 'nombre': 'Hombre' },
        { 'id': 'F', 'nombre': 'Mujer' },
        { 'id': 'U', 'nombre': 'Unisex' },
    ]

    show_popover(0, 0)
    show_breadcrum()
    set_filters(marcas, filtro_marcas, 'marca')
    set_filters(tallas, filtro_tallas, 'talla')
    set_filters(sexos, filtro_sexos, 'sexo')
    productos_filtrados_por_tipo = productos
    productos_filtrados = productos_filtrados_por_tipo
    set_products_section_title(titulo, productos_filtrados)
    show_filter_products();

    var checkboxes = document.querySelectorAll("input[type=checkbox]");


    // Use Array.forEach to add an event listener to each checkbox.
    checkboxes.forEach(function (checkbox) {
        checkbox.addEventListener('change', function () {

            let filter_type = this.name
            let filter_name = this.value
            if (this.checked) {
                /*let url = `../controlador/filtrar_productos.php?filter=${filter_type}&value=${filter_name}`
                fetch(url, {
                    method: 'GET',
                    mode: 'cors'
                }).then(response => response.json())
                    .then(data => {
                        console.log(data)
                    })*/
                productos_filtrados = productos_filtrados_por_tipo.filter(producto => producto[filter_type] == filter_name);
                checkboxes.forEach(checkbox => {
                    if (checkbox.value != filter_name) {
                        checkbox.checked = false;
                    }
                })


            } else {
                productos_filtrados = productos
            }
            products_container.innerHTML = "";
            set_products_section_title(titulo, productos_filtrados)
            show_filter_products()
        })
    });

    ropa_boton.addEventListener('click', e => {
        show_popover(e.x, e.y + 10)
    })

    var popover_options = document.querySelectorAll("li[class=popover-option]");
    popover_options.forEach(popover => {
        popover.addEventListener('click', e => {
            let product_type = e.srcElement.id;
            let new_title = e.srcElement.innerHTML;
            breadcrum_niveles = ["Inicio", "Ropa"]
            if (product_type != "ALL") {
                productos_filtrados_por_tipo = productos.filter(producto => producto.id_categoria == product_type);
                titulo = new_title;
                breadcrum_niveles.push(capital_letter(new_title.toLowerCase()))
            } else {
                productos_filtrados_por_tipo = productos;
                titulo = "ROPA"
            }
            productos_filtrados = productos_filtrados_por_tipo
            products_container.innerHTML = "";
            show_breadcrum()
            set_products_section_title(titulo, productos_filtrados)
            show_filter_products()
        })
    })
}

async function getProducts() {
    let url = `../controlador/productos.php`
    const response = await fetch(url, {
        method: 'GET',
        mode: 'cors'
    }).then(res => res.json())
    return response;

}

function show_breadcrum() {
    breadcrum.innerHTML = "";
    let i = 1;
    let body = "";
    breadcrum_niveles.forEach(nivel => {
        let tag = "";
        if (i == breadcrum_niveles.length) {
            tag = '<li>' + nivel + '</li>'
        } else {
            tag = '<li><a href="#">' + nivel + '</a></li>'
        }
        body += tag;
        i++;
    })
    breadcrum.innerHTML = body;

}

function product_to_tag(product) {
    tag_text = '<div class="col">' +
        '<div class="card shadow-sm">' +
        '<img src="../images/productos/' + product.id + '/principal.jpeg" alt="">' +
        '<div class="card-body">' +
        '<h5 class="card-title">' + product.marca + '</h5>' +
        '<p class="card-text">' + product.nombre + '</p>' +
        '<p class="card-text">S/.' + parseFloat(product.precio).toFixed(2) + '</p>' +
        '<div class="d-flex justify-content-between align-items-center">' +
        '<div class="btn-group">' +
        '<a href="../detalles.php?id=' + product.id + '&token='+product.token+'" class="btn btn-primary">Detalles</a>' +
        '</div>' +
        `<button class="btn btn-outline-success" type="button" onclick="addProducto( ${product.id},'${product.token}')">Agregar al carrito</button>`+
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';
    tag = document.createElement("div");
    tag.innerHTML = tag_text;
    return tag
}

function set_products_section_title(title, filter_products) {
    products_title.innerHTML = "";
    tag = document.createElement("div");
    tag.style.display = 'flex';
    total_products = filter_products.length;
    let x = " productos"
    if (total_products == 1) {
        x = " producto"
    }
    tag.innerHTML = "<h2>" + title + "</h2><h4 class=\"subtitulo_productos\">" + total_products + "" + x + " </h4>";
    products_title.appendChild(tag)
}

function set_filters(filters, filter_tag, filter_type) {
    filters.forEach(filter => {
        tag = document.createElement("div");
        tag.className = 'filtro'
        tag.innerHTML = '<label><input type="checkbox" name="' + filter_type + '" value="' + filter.id + '">' + filter.nombre + '</label><br>'
        filter_tag.appendChild(tag);
    });
}

function show_filter_products() {
    productos_filtrados.forEach(producto => {
        const product_tag = product_to_tag(producto);
        products_container.appendChild(product_tag);
    });
}

function show_popover(x, y) {
    if (popover_is_showed) {
        popover.style.left = x + 'px';
        popover.style.top = y + 'px';
        popover.style.display = 'flex'
        popover_is_showed = false
    } else {
        popover.style.display = 'none'
        popover_is_showed = true
    }
}

function capital_letter(text) {
    let first_letter = text.substring(0, 1);
    let incomplete_text = text.substring(1);
    return first_letter.toUpperCase() + incomplete_text;
}

function getBrands(products) {
    let brands = []
    
    products.forEach((product) => {
        brands.push(product['marca'])
    })
    brands= new Set(brands);
    brands= Array.from(brands);
    brands= brands.map((brand)=>{
        return { 'id': brand, 'nombre':brand  };
    })
    return brands;
}

// Variables

products_container = document.getElementById("products-container");
products_title = document.getElementById("products-title");
filtro_marcas = document.getElementById("filtro-marcas");
filtro_tallas = document.getElementById("filtro-talla")
filtro_sexos = document.getElementById("filtro-sexo");
ropa_boton = document.getElementById("ropa-button");
let popover = document.getElementById("products-popover");
let breadcrum = document.getElementById("breadcrumb");
let breadcrum_niveles;
let popover_is_showed;
let titulo = "ROPA";
let productos_filtrados;
let productos_filtrados_por_tipo;



// INICIALIZADOR
initPage();

// ESCUCHADORES DE EVENTO
