//------------------------------------------------- BUSCADOR -----------------------------------------------------

//-------------- EJECUTANDO FUNCIONES ------------
document.getElementById("search").addEventListener("click", mostrarBuscador);
document.getElementById("cover-ctn-search").addEventListener("click", ocultarBuscador);



// -------VARIABLES:--------
caja_busqueda   = document.getElementById("ctn-bars-search");
fondo_busqueda  = document.getElementById("cover-ctn-search");
input_busqueda  = document.getElementById("input-search");
box_busqueda    = document.getElementById("box-search");

//---- FUNCION PARA MOSTRAR BUSCADOR ----
function mostrarBuscador(){
    caja_busqueda.style.top = "100px";
    fondo_busqueda.style.display = "block";
    input_busqueda.focus();

    if (input_busqueda.value === ""){
        box_busqueda.style.display = "none";
    }
}

//---- FUNCION PARA OCULTAR BUSCADOR ----
function ocultarBuscador(){
    caja_busqueda.style.top = "-100px";
    fondo_busqueda.style.display = "none";
    input_busqueda.value = "";
    box_busqueda.style.display = "none";
}

// //----- CREAR FILTRADO DE BUSQUEDA -----
// document.getElementById("input_busqueda").addEventListener("keyup", buscadorInterno);

// function buscadorInterno(){
//     filter = input_busqueda.value.toUpperCase();
//     li = box_busqueda.getElementsByTagName("li");

//     //-------RECORRIENDO ELEMENTOS DE PRODUCTO A FILTRAR DE LOS Li ------------
//     for(i = 0; i < li.length; i++){
//         a = li[i].getElementsByTagName("a")[0];
//         textValue = a.textContent || a.innerText;         //va a buscar lo que escribes y lo compara con lo que hay en Li

//         if (textValue.toUpperCase().indexOf(filter) > -1){      //indexar de manera organizada los valores dl Li
//             li[i].style.display = "";                // Desactivamos el display none de los Li si encuentra el nombre
//             box_busqueda.style.display = "block";

//             if (input_busqueda.value === ""){
//                 box_busqueda.style.display = "none";
//             }

//         }else{
//             li[i].style.display = "none";            // Activamos el display none de los Li ya que no hay resultados iguales
//         }
//     }
// }

