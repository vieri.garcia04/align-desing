<?php
    require 'modelo/config.php';
    require 'modelo/database.php';
    $db = new Database();
    $con = $db->conectar();

    $productos = isset($_SESSION['carrito']['productos']) ? $_SESSION['carrito']['productos'] : null;

    print_r($_SESSION);

    $lista_carrito = array();

    if($productos != null){
        foreach ($productos as $clave => $cantidad){
            $sql = $con->prepare("SELECT id, nombre, marca, precio, descuento, $cantidad as cantidad FROM productos WHERE id=? AND activo=1");

            $sql->execute([$clave]);
            $lista_carrito[] = $sql->fetch(PDO::FETCH_ASSOC);
        }
    }else{
        header("location: index.php");
        exit;
    }
    
?>


<!DOCTYPE html>
<html lang="es">
<>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">

    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;400&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Anton&family=Kanit:wght@100;400&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cinzel:wght@700;800&family=Montserrat&display=swap" rel="stylesheet">
    
    <script rel="stylesheet"  src="https://kit.fontawesome.com/c174601175.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="./css/header.css">
    <link rel="stylesheet" type="text/css" href="./css/header.css">
    <link rel="stylesheet" href="./css/footer.css">
    <link rel="stylesheet" type="text/css" href="./css/footer.css">
    <link rel="stylesheet" href="./css/pago.css">
    <link rel="stylesheet" type="text/css" href="./css/pago.css">
    <link rel="stylesheet" href="./css/FontAwesome/all.min.css">
    <link rel="icon" type="image/png" href="./img/logo-alignStyle.png">

    <link href="css/estilos.css" rel="stylesheet">

    <title>Tienda</title>
</head>
<body>
    <header>
        <div class="descuento">
            <h6>ENVIOS GRATUITOS POR COMPRAS MAYORES A S/ 299 SOLES</h6>
        </div>
        <nav class="contenedor_menu">
            <input type="checkbox" id="check">
            <label for="check" class="checkbtn"><i class="fa-solid fa-bars"></i></label>
            <a class="titulo" href="./index.php" target="_top">ALIGN STYLE</a>

            <ul class="ul-header">
                <li><a class="texto" href="./pages/zapatillas.html" target="_top">ZAPATILLAS <i
                            class="fa-solid fa-chevron-down"></i></a></li>
                <li><a class="texto" href="./pages/ropa.php" target="_top">ROPA <i
                            class="fa-solid fa-chevron-down"></i></a></li>
                <li><a class="texto" href="./pages/accesorios.html" target="_top">ACCESORIOS <i
                            class="fa-solid fa-chevron-down"></i></a></li>
                <li><a class="texto" href="./pages/marcas.html" target="_top">MARCAS <i
                            class="fa-solid fa-chevron-down"></i></a>
                    <ul class="sub_menu">
                        <li><a href="./pages/adidas.html"><img class="img_prod" src="./img/marcas/adidas.jpeg"
                                    alt=""></a></li>
                        <li><a href=""><img class="img_prod" src="./img/marcas/nb.jpeg" alt=""></a></li>
                        <li><a href=""><img class="img_prod" src="./img/marcas/puma.jpeg" alt=""></a></li>
                        <li><a href=""><img class="img_prod" src="./img/marcas/nike.jpeg" alt=""></a></li>
                        <li><a href=""><img class="img_prod" src="./img/marcas/converse.jpeg" alt=""></a></li>
                        <li><a href=""><img class="img_prod" src="./img/marcas/champion.jpeg" alt=""></a></li>
                        <li><a href=""><img class="img_prod" src="./img/marcas/crocs.jpeg" alt=""></a></li>
                        <li><a href=""><img class="img_prod" src="./img/marcas/kappa.jpeg" alt=""></a></li>
                        <li><a href=""><img class="img_prod" src="./img/marcas/ne.jpeg" alt=""></a></li>
                        <li><a href=""><img class="img_prod" src="./img/marcas/rebels.jpeg" alt=""></a></li>
                    </ul>
                </li>
            </ul>

            <div>

                <i id="search" class="fa-solid fa-magnifying-glass logos buscar" target="_top"></i> <!-- Busqueda-->
                <a class="logos login" href="./pages/miCuenta.php" target="_top"><i
                        class="fa-solid fa-user"></i></a>
                <!--Inicio Sesión -->
                <a class="logos bolsa" href="checkout.php" target="_top"><i
                        class="fa-solid fa-bag-shopping"></i><span id="num_cart" class="badge bg-secondary contador"><?php echo $num_cart; ?></span></a>
                <!--Carrito-->
            </div>
        </nav>

        <!-- ********************************  BUSCADOR  *******************************+ -->
        <div class="ctn-bars-search" id="ctn-bars-search" action="">
            <input class="buscar-input" id="input-search" type="text" placeholder="Buscar">
        </div>

        <ul id="box-search">
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Polo</a></li>
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Zapatilla</a></li>
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Pantalon</a></li>
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Camisa</a></li>
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Gorro</a></li>
        </ul>

        <div id="cover-ctn-search"></div>
        <!-- ***************************************************************************** -->
    </header>

    <main>
        <div class="container">

            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <h4>Detalles de pago</h4>
                    

                    <div class="col-6">
                        <div class="table-responsive">
                            <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Producto</th>
                                            <th>Subtotal</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php if($lista_carrito == null){
                                            echo '<tr><td colspan="5" class="text-center"><b>Lista vacia</b></td></tr>';
                                        } else {
                                            
                                            $total = 0;
                                            foreach($lista_carrito as $producto){
                                                $_id = $producto['id'];
                                                $nombre = $producto['nombre'];
                                                $precio = $producto['precio'];
                                                $descuento = $producto['descuento'];
                                                $cantidad = $producto['cantidad'];
                                                $precio_desc = $precio - (($precio * $descuento) / 100);
                                                $subtotal = $cantidad * $precio_desc;
                                                $total += $subtotal;
                                            ?>
                                                <tr>
                                                    <td><?php echo $nombre; ?></td>

                                                    <td>
                                                        <div id="subtotal_<?php echo $_id; ?>" name="subtotal[]"><?php echo MONEDA . number_format($subtotal,2, '.', ','); ?></div>
                                                    </td>

                                                </tr>
                                            <?php } ?>

                                                <tr>
                                                    <td colspan="2">
                                                        <p class="h3 text-end" id="total"><?php echo MONEDA . number_format($total, 2, '.', ','); ?>
                                                    </td>
                                                </tr>

                                    </tbody>
                                        <?php } ?>
                            </table>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6" id="paypal-button-container"></div>
                </div>
            </div>
        </div>
    </main>

   

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>

    <script src="https://www.paypal.com/sdk/js?client-id=<?php echo CLIENT_ID; ?>&currency=<?php echo CURRENCY; ?>"></script>

    <script>
        paypal.Buttons({
            style:{
                color: 'blue',
                chape: 'pill',
                label: 'pay'
            },
            createOrder: function(data, actions){
                return   actions.order.create({
                    purchase_units: [{
                        amount: {
                            value: <?php echo $total; ?>
                        }
                    }]
                });
            },

            onApprove: function(data, actions){
                let URL = 'controlador/captura.php'
                actions.order.capture().then(function(detalles){
                    // window.location.href="completado.html";
                    console.log(detalles)

                    let url = 'controlador/captura.php'

                    return fetch(url, {
                        method: 'post',
                        headers: {
                            'content-type': 'application/json'
                        },
                        body: JSON.stringify({
                            detalles: detalles
                        })
                    }).then(function(response){
                        window.location.href = "completado.php?key=" +detalles['id'];    //$datos['detalles']['id']
                    })
                });
            },

            onCancel: function(data){
                alert("Pago cancelado");
                console.log(data);
            }
        }).render('#paypal-button-container');
    </script>
    <script src="https://account.snatchbot.me/script.js"></script><script>window.sntchChat.Init(252214)</script> 

</body>
</html>