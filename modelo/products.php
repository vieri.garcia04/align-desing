
<?php
    require 'database.php';

    class ProductModel{


        function getAllProducts(){
            $db = new Database();
            $con = $db->conectar();

            $sql = "SELECT * FROM productos";

            try {
                $query=$con->query($sql);
                $query->execute();
                $products = array();
                while($row = $query->fetch(PDO::FETCH_ASSOC))
                {
                    $token=hash_hmac('sha1',$row['id'], KEY_TOKEN);
                    $row['token']=$token;
                    $products[] =$row;

                }
                return $products;
            } catch (Exception $e) {
                return [];
            }
        }

        function getProductsByFilter($filter, $value){
            $db = new Database();
            $con = $db->conectar();

            $sql = "SELECT * FROM productos WHERE ".$filter. "=\"". $value."\"";

            try {
                $query=$con->query($sql);
                $query->execute();
                $products = array();
                while($row = $query->fetch(PDO::FETCH_ASSOC))
                {
                    $products[] =$row;

                }
                return $products;
            } catch (Exception $e) {
                return [];
            }
        }
    }
      
       
?>