
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;400&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Anton&family=Kanit:wght@100;400&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cinzel:wght@700;800&family=Montserrat&display=swap" rel="stylesheet">
    
    <script rel="stylesheet"  src="https://kit.fontawesome.com/c174601175.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="../css/header.css">
    <link rel="stylesheet" type="text/css" href="../css/header.css">
    <link rel="stylesheet" href="../css/footer.css">
    <link rel="stylesheet" type="text/css" href="../css/footer.css">
    <link rel="stylesheet" href="../css/direcciones.css">
    <link rel="stylesheet" type="text/css" href="../css/direcciones.css">
    <title>Direcciones</title>
</head>

<?php
    error_reporting(0);
    if (isset($_POST['continuar'])) {
        $nombre=$_POST['nombre'];
        $apellido=$_POST['apellido'];
    }        
?>

<body>

    <header>
        <div class="descuento">
            <h6>ENVIOS GRATUITOS POR COMPRAS MAYORES A S/ 299 SOLES</h6>
        </div>
        <nav class="contenedor_menu">
            <input type="checkbox" id="check">
            <label for="check" class="checkbtn"><i class="fa-solid fa-bars"></i></label>
            <a class="titulo" href="../index.php" target="_top">ALIGN STYLE</a>

            <ul class="ul-header">
                <li><a class="texto" href="../pages/zapatillas.html" target="_top">ZAPATILLAS <i class="fa-solid fa-chevron-down"></i></a></li>
                <li><a class="texto" href="../pages/ropa.php" target="_top">ROPA <i class="fa-solid fa-chevron-down"></i></a></li>
                <li><a class="texto" href="../pages/accesorios.html" target="_top">ACCESORIOS <i class="fa-solid fa-chevron-down"></i></a></li>
                <li><a class="texto" href="../pages/marcas.html" target="_top">MARCAS <i class="fa-solid fa-chevron-down"></i></a></li>
            </ul>
        
            <div>
                
                <i id="search" class="fa-solid fa-magnifying-glass logos buscar" target="_top"></i>    <!-- Busqueda-->
                <a class="logos login" href="miCuenta.php" target="_top"><i class="fa-solid fa-user"></i></a><!--Inicio Sesión -->
                <a class="logos bolsa" href="../checkout.php" target="_top"><i class="fa-solid fa-bag-shopping"></i></a><!--Carrito-->

            </div>
        </nav>

        <!-- ********************************  BUSCADOR  *******************************+ -->
        <div class="ctn-bars-search" id="ctn-bars-search" action="">
            <input class="buscar-input" id="input-search" type="text" placeholder="Buscar">
        </div>

        <ul id="box-search">
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Polo</a></li>
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Zapatilla</a></li>
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Pantalon</a></li>
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Camisa</a></li>
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Gorro</a></li>
        </ul>

        <div id="cover-ctn-search"></div>
        <!-- ***************************************************************************** -->
    </header>

    

    <div class="flex-container">
        <div action="" class="container">

            <div class="form__section">
                <h2 class="izq desactiva">&nbsp;&nbsp;&nbsp;✔ DATOS PERSONALES&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h2>
                <a class="modificar" href="../pages/datosPersonales.html">modificar</a>
            </div>

            <form action="../pages/envios.html" class="form">

                <div class="form__section1">
                    <h2 class="izq tituloDirecciones">&nbsp;&nbsp;2. DIRECCIONES</h2>
                </div>

                <div class="form__section1">
                        <p class="mensaje_direccion">La dirección seleccionada se utilizará tanto como de dirección personal (para la factura) como de dirección de entrega</p>
                </div>
               
                <div class="form__section1">
                    <label class="nomb" for="">&nbsp;&nbsp;&nbsp;Nombre &nbsp;&nbsp;&nbsp;</label>
                    <input class="form__input name" type="text" name="nombre" id="nombre" min="0" max="30" value="<?php echo $nombre ?> "  disabled>
                </div>

                <div class="form__section1">
                    <label class="apellido" for="">&nbsp;&nbsp;&nbsp;Apellido &nbsp;&nbsp;&nbsp;</label>
                    <input class="form__input apell" type="text" name="apellido" id="apellido" min="0" max="50"  value="<?php echo $apellido ?> " disabled>
                </div>

                <div class="form__section1">
                    <label class="dniLabel" for="">&nbsp;&nbsp;&nbsp;DNI&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <input class="form__input dni" type="text" name="" id="dni" maxlength="8" minlength="8"  placeholder="Ingrese su DNI" required pattern="[0-9]+">
                </div>

                <div class="form__section1">
                    <label class="celular" for="">&nbsp;&nbsp;&nbsp;Celular&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <input class="form__input cel" type="text" name="" id="celular" maxlength="9" minlength="9"  placeholder="Ingrese su celular" required pattern="[0-9]+">
                </div>
                
                <div class="form__section1">
                    <label class="direccion" for="">&nbsp;&nbsp;&nbsp;Direccion&nbsp;&nbsp;</label>
                    <input class="form__input direc"type="text" name="" id="direccion" maxlength="100" minlength="0"  placeholder="Ingrese su domicilio" required >
                </div>

                <div class="form__section1">
                    <label class="referencia" for="">&nbsp;&nbsp;&nbsp;Referencia</label>
                    <input class="form__input ref" type="text" name="" id="referencia" maxlength="100" minlength="0"  placeholder="Ingrese la referencia" >
                    <p class="opcional">&nbsp;&nbsp;opcional</p>

                </div>

                <div class="form_section1">
                    <label class="distrito" for="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Distrito</label>
                    <select class="form__input seleccionar" name="" id="">
                        <option class="seleccione" value="" disabled selected> - - Por favor, seleccione - -</option>
                        <option value="Ancón">Ancón</option>
                        <option value="Ate Vitarte">Ate Vitarte</option>
                        <option value="Barranco">Barranco</option>
                        <option value="Breña">Breña</option>
                        <option value="Carabayllo">Carabayllo</option>
                        <option value="Chaclacayo">Chaclacayo</option>
                        <option value="Chorrillos">Chorrillos</option>
                        <option value="Cieneguilla">Cieneguilla</option>
                        <option value="Comas">Comas</option>
                        <option value="El Agustino">El Agustino</option>
                        <option value="Independencia">Independencia</option>
                        <option value="Jesús María">Jesús María</option>
                        <option value="La Molina">La Molina</option>
                        <option value="La Victoria">La Victoria</option>
                        <option value="Lima">Lima</option>
                        <option value="Lince">Lince</option>
                        <option value="Los Olivos">Los Olivos</option>
                        <option value="Lurigancho">Lurigancho</option>
                        <option value="Lurín">Lurín</option>
                        <option value="Magdalena del Mar">Magdalena del Mar</option>
                        <option value="Miraflores">Miraflores</option>
                        <option value="Pachacamac">Pachacamac</option>
                        <option value="Pucusana">Pucusana</option>
                        <option value="Pueblo Libre">Pueblo Libre</option>
                        <option value="Puente Piedra">Puente Piedra</option>
                        <option value="Punta Hermosa">Punta Hermosa</option>
                        <option value="Punta Negra">Punta Negra</option>
                        <option value="Rímac">Rímac</option>
                        <option value="San Bartolo">San Bartolo</option>
                        <option value="San Borja">San Borja</option>
                        <option value="San Isidro">San Isidro</option>
                        <option value="San Juan de Lurigancho">San Juan de Lurigancho</option>
                        <option value="San Juan de Miraflores">San Juan de Miraflores</option>
                        <option value="San Luis">San Luis</option>
                        <option value="San Martín de Porres">San Martín de Porres</option>
                        <option value="San Miguel">San Miguel</option>
                        <option value="Santa Anita">Santa Anita</option>
                        <option value="Santa María del Mar">Santa María del Mar</option>
                        <option value="Santa Rosa">Santa Rosa</option>
                        <option value="Santiago de Surco">Santiago de Surco</option>
                        <option value="Surquillo">Surquillo</option>
                        <option value="Villa El Salvador">Villa El Salvador</option>
                        <option value="Villa María del Triunfo">Villa María del Triunfo</option>
                        <option value="otros">Otros...</option>
                    </select>
                </div>  

                <div class="form_section1">
                    <button class="continuar"><b>CONTINUAR</b></button>
                </div>
            </form>
        
            <div class="form__section">
                <h2 class="izq desactiva ">&nbsp;&nbsp;&nbsp;3. METODOS DE ENVIO</h2>
            </div>

            <div class="form__section">
                <h2 class="izq desactiva">&nbsp;&nbsp;&nbsp;4. PAGO</h2>
            </div>
            
        </div>
            
        <div class="container">
            <div class="form__section cantidades princ">
                <label class="izq" for="">0 articulos</label>       
                <label class="der transGratis" for="">S/. 0.00</label>
            </div>
            <div class="form__section cantidades">
                <label class="izq" for="">Transporte</label>
                <label class="der transGratis" for="">Gratis</label>
            </div>
            <div class="form__section totales">
                <label class="izq" for="">Total (impuestos inc.)</label>       
                <label class="der transGratis" for="">S/. 0.00</label>            </div>
        
            <div class="form__section imagenes">
                <div class="contenido">
                    <img class="imagen" src="../img/carrito/bloquear.png" alt="">
                    <p class="mensaje"><b>COMPRA SEGURA</b><br>Con nuestro certificado SSL tus datos están protegidos</p>
                </div>
                <div class="contenido">
                    <img class="imagen" src="../img/carrito/envio-gratis.png" alt="">
                    <p class="mensaje"><b>ENVIO GRATIS</b><br>Por compras mayores a  S/ 299 soles</p>
                </div>
                
                <div class="contenido">
                    <img class="imagen" src="../img/carrito/paquete.png" alt="">
                    <p class="mensaje"><b>CAMBIOS Y DEVOLUCIONES</b><br>Puedes solicitar cambio de producto dentro del plazo 
                    establecido</p>
                </div>
            </div>
        </div>
          
        
    </div>

    <footer>
        <div class="datosencabezado"> <!-- Encabezado de footer -->
            <div class="seccionencabezado">
                <a class="titulo" href="./index.php" target="_top">ALIGN STYLE</a> <!-- ALIGN STYLE -->
            </div>
            <div class="seccionencabezado">
                <a class="subtitulo">TIENDA TRUJILLO</a>
            </div>
            <div class="seccionencabezado">
                <a class="subtitulo">TIENDA CHICLAYO</a>
            </div>
            <div class="seccionencabezado">
                <a class="subtitulo">NUESTRA EMPRESA</a>
            </div>
        </div>
        <div class="datos"> <!-- datos de footer -->
            <div class="seccion">
                <div class="secciondatos">
                    <a class="logodatos"><i class="fa-solid fa-location-dot"></i></a> <!-- icono gps -->
                    <div class="texto">
                        Primer Nivel <br>
                        Barrio Jocker Plaza <br>
                        Encuéntranos frente a <br>
                        Calvin Klein
                    </div>
                </div> <br>
                <div class="secciondatos">
                    <a class="logodatos"><i class="fa-solid fa-clock"></i></a><!-- Icono de reloj -->
                    <div class="texto">
                        Horario de atención: <br>
                        10:00 am - 10:00 pm <br>
                        Perú
                    </div>
                </div> <br>
                <div class="secciondatos">
                    <a class="logodatos"><i class="fa-solid fa-phone"></i></a> <!-- Icono de telefono -->
                    <div class="texto">
                        966 677 227 <br>
                        Atención al cliente
                    </div>
                </div> <br>
                <div class="secciondatos">
                    <a class="logodatos"><i class="fa-solid fa-envelope"></i></a><!-- Icono de mail -->
                    <div class="texto">
                        soporte@alignstyle.pe
                    </div>
                </div>
            </div>
            <div class="seccion">
                <div class="secciondatos">
                    <a class="logodatos"><i class="fa-solid fa-location-dot"></i></a> <!-- icono gps -->
                    <div class="texto">
                        Av. Húsares de junín 316 <br>
                        La Merced
                    </div>
                </div> <br>
                <div class="secciondatos">
                    <a class="logodatos"><i class="fa-solid fa-clock"></i></a><!-- Icono de reloj -->
                    <div class="texto">
                        Horario de atención: <br>
                        Lunes a Domingo <br>
                        10:00 am - 9:00 pm <br>
                        Perú
                    </div>
                </div>
            </div>
            <div class="seccion">
                <div class="secciondatos">
                    <a class="logodatos"><i class="fa-solid fa-location-dot"></i></a> <!-- icono gps -->
                    <div class="texto">
                        Calle Alfonso Ugarte 850
                    </div>
                </div> <br>
                <div class="secciondatos">
                    <a class="logodatos"><i class="fa-solid fa-clock"></i></a><!-- Icono de reloj -->
                    <div class="texto">
                        Horario de atención: <br>
                        Lunes a Sábado <br>
                        10:00 am - 09:00 pm <br>
                        Domingos <br>
                        11:00 am - 7:00 pm <br>
                        Perú
                    </div>
                </div>
            </div>
            <div class="seccion">
                <div class="secciondatos">
                    <a class="logodatos"></a>
                    <div class="texto">
                        <a class="texto">Política de privacidad</a><br> <!-- Target Top para que se cargue la pagina otra vez y no se cree un cuadro con la otra ventana -->
                        <a class="texto">Términos y condiciones</a> <br>
                        <a class="texto">Tiendas</a><br>
                        <a class="texto" href="./pages/nosotros.html" target="_top">Nosotros</a><br>
                        <a class="texto" href="./pages/equipo.html" target="_top">Equipo</a><br>
                        <a class="texto" href="./pages/contactanos.html" target="_top">Contáctanos</a><br>
                        <a class="texto" href="./pages/servicios.html" target="_top">Servicios</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright">
            <div class="copy">
                ALIGN STYLE &copy; 2022, ALL RIGHT RESERVERED
            </div>
            <div class="info">
                Mas información: 
                <a class="logos-a" href="https://www.facebook.com/ALIGN-STYLE-100589032692689" target="_blank"><i class="fa-brands fa-facebook"></i></a>
                <a class="logos-a" href="https://www.instagram.com/align.style/" target="_blank"><i class="fa-brands fa-instagram"></i></a>
                <a class="logos-a" href="https://wa.me/message/4W4MPALEQJZGE1" target="_blank"><i class="fa-brands fa-whatsapp"></i></a>
                <a class="logos-a" href="https://vm.tiktok.com/ZMNRk35WR/" target="_blank"><i class="fa-brands fa-tiktok"></i></a>
            </div>
        </div>
    </footer>
    <script src="../js/header.js"></script>   

</body>
</html>
