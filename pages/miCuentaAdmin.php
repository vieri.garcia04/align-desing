<!DOCTYPE html>
<html lang="en">
<?php
    session_start();
    $isLogin=$_SESSION["isLogin"];
    $user=$_SESSION["user"];
?>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;400&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Anton&family=Kanit:wght@100;400&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cinzel:wght@700;800&family=Montserrat&display=swap" rel="stylesheet">
    
    <script rel="stylesheet"  src="https://kit.fontawesome.com/c174601175.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="../css/header.css">
    <link rel="stylesheet" type="text/css" href="../css/header.css">
    <link rel="stylesheet" href="../css/footer.css">
    <link rel="stylesheet" type="text/css" href="../css/footer.css">
    <link rel="stylesheet" href="../css/iniciarSesion.css">
    <link rel="stylesheet" type="text/css" href="../css/iniciarSesion.css">
    <link rel="icon" type="image/png" href="../img/logo-alignStyle.png">
    <title>Iniciar Sesion</title>
</head>
<body>
    <!-- ********** MENÚ ********** -->
    <header>
        <div class="descuento">
            <h6>ENVIOS GRATUITOS POR COMPRAS MAYORES A S/ 299 SOLES</h6>
        </div>
        <nav class="contenedor_menu">
            <input type="checkbox" id="check">
            <label for="check" class="checkbtn"><i class="fa-solid fa-bars"></i></label>
            <a class="titulo" href="../index.php" target="_top">ALIGN STYLE</a>

            <ul class="ul-header">
                <li><a class="texto" href="../pages/zapatillas.html" target="_top">ZAPATILLAS <i class="fa-solid fa-chevron-down"></i></a></li>
                <li><a class="texto" href="../pages/ropa.php" target="_top">ROPA <i class="fa-solid fa-chevron-down"></i></a></li>
                <li><a class="texto" href="../pages/accesorios.html" target="_top">ACCESORIOS <i class="fa-solid fa-chevron-down"></i></a></li>
                <li><a class="texto" href="../pages/marcas.html" target="_top">MARCAS <i class="fa-solid fa-chevron-down"></i></a></li>
            </ul>
        
            <div>
                
                <i id="search" class="fa-solid fa-magnifying-glass logos buscar" target="_top"></i>    <!-- Busqueda-->
                <?php
                    if ($isLogin){
                        echo '<a class="logos login" href="./miCuenta.php" target="_top"><i class="fa-solid fa-user"></i></a>';
                    }else{
                        echo '<a class="logos login" href="../pages/iniciarSesion.php" target="_top"><i class="fa-solid fa-user"></i></a>';
                    }
                    
                ?>
                 <!--Inicio Sesión -->
                <a class="logos bolsa" href="../checkout.php" target="_top"><i class="fa-solid fa-bag-shopping"></i></a><!--Carrito-->
            </div>
        </nav>

        <!-- ********************************  BUSCADOR  *******************************+ -->
        <div class="ctn-bars-search" id="ctn-bars-search" action="">
            <input class="buscar-input" id="input-search" type="text" placeholder="Buscar">
        </div>

        <ul id="box-search">
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Polo</a></li>
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Zapatilla</a></li>
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Pantalon</a></li>
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Camisa</a></li>
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Gorro</a></li>
        </ul>

        <div id="cover-ctn-search"></div>
        <!-- ***************************************************************************** -->
    </header>

    <nav class="nave-crearCuenta">
        <a class="navegador" href="../index.php"><b class="textNavegador">Inicio</b></a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <a class="navegador" href="../pages/iniciarSesion.php"><b class="textNavegador">Mi cuenta</b></a> &nbsp;    
    </nav>
    
    <h1 class="titulo-iniciarSesion">Mi cuenta</h1>
    <div class="flex-container ">
        <h3>Administrador</h3><br>
        <?php
       /*  isset($user['name']);
        $txtname = $user['name'];
        isset($user['lastname']);
        $txtlastname = $user['lastname']; */
            echo strtoupper($user['name'])." ".strtoupper($user['lastname'])
        ?>
    </div>

    <h1 class="otros-titulos">Usuarios</h1>
    <div class="flex-container">
        <h3>Lista de usuarios</h3><br>
        <?php
        /* isset($user['name']);
        $txtname = $user['name'];
        isset($user['lastname']);
        $txtlastname = $user['lastname'] */;
        echo strtoupper($user['name'])." ".strtoupper($user['lastname'])
        ?>
    </div>

    <h1 class="otros-titulos">Bandeja de Mensajes</h1>
    <div class="flex-container-mensajes">
        <h3>Usuario</h3>

        <h4>Mensaje</h4>

    </div>

    <footer>
        <div class="datosencabezado"> <!-- Encabezado de footer -->
            <div class="seccionencabezado">
                <a class="titulo" href="./index.php" target="_top">ALIGN STYLE</a> <!-- ALIGN STYLE -->
            </div>
            <div class="seccionencabezado">
                <a class="subtitulo">TIENDA TRUJILLO</a>
            </div>
            <div class="seccionencabezado">
                <a class="subtitulo">TIENDA CHICLAYO</a>
            </div>
            <div class="seccionencabezado">
                <a class="subtitulo">NUESTRA EMPRESA</a>
            </div>
        </div>
        <div class="datos"> <!-- datos de footer -->
            <div class="seccion">
                <div class="secciondatos">
                    <a class="logodatos"><i class="fa-solid fa-location-dot"></i></a> <!-- icono gps -->
                    <div class="texto">
                        Primer Nivel <br>
                        Barrio Jocker Plaza <br>
                        Encuéntranos frente a <br>
                        Calvin Klein
                    </div>
                </div> <br>
                <div class="secciondatos">
                    <a class="logodatos"><i class="fa-solid fa-clock"></i></a><!-- Icono de reloj -->
                    <div class="texto">
                        Horario de atención: <br>
                        10:00 am - 10:00 pm <br>
                        Perú
                    </div>
                </div> <br>
                <div class="secciondatos">
                    <a class="logodatos"><i class="fa-solid fa-phone"></i></a> <!-- Icono de telefono -->
                    <div class="texto">
                        966 677 227 <br>
                        Atención al cliente
                    </div>
                </div> <br>
                <div class="secciondatos">
                    <a class="logodatos"><i class="fa-solid fa-envelope"></i></a><!-- Icono de mail -->
                    <div class="texto">
                        soporte@alignstyle.pe
                    </div>
                </div>
            </div>
            <div class="seccion">
                <div class="secciondatos">
                    <a class="logodatos"><i class="fa-solid fa-location-dot"></i></a> <!-- icono gps -->
                    <div class="texto">
                        Av. Húsares de junín 316 <br>
                        La Merced
                    </div>
                </div> <br>
                <div class="secciondatos">
                    <a class="logodatos"><i class="fa-solid fa-clock"></i></a><!-- Icono de reloj -->
                    <div class="texto">
                        Horario de atención: <br>
                        Lunes a Domingo <br>
                        10:00 am - 9:00 pm <br>
                        Perú
                    </div>
                </div>
            </div>
            <div class="seccion">
                <div class="secciondatos">
                    <a class="logodatos"><i class="fa-solid fa-location-dot"></i></a> <!-- icono gps -->
                    <div class="texto">
                        Calle Alfonso Ugarte 850
                    </div>
                </div> <br>
                <div class="secciondatos">
                    <a class="logodatos"><i class="fa-solid fa-clock"></i></a><!-- Icono de reloj -->
                    <div class="texto">
                        Horario de atención: <br>
                        Lunes a Sábado <br>
                        10:00 am - 09:00 pm <br>
                        Domingos <br>
                        11:00 am - 7:00 pm <br>
                        Perú
                    </div>
                </div>
            </div>
            <div class="seccion">
                <div class="secciondatos">
                    <a class="logodatos"></a>
                    <div class="texto">
                        <a class="texto">Política de privacidad</a><br> <!-- Target Top para que se cargue la pagina otra vez y no se cree un cuadro con la otra ventana -->
                        <a class="texto">Términos y condiciones</a> <br>
                        <a class="texto">Tiendas</a><br>
                        <a class="texto" href="./pages/nosotros.html" target="_top">Nosotros</a><br>
                        <a class="texto" href="./pages/equipo.html" target="_top">Equipo</a><br>
                        <a class="texto" href="./pages/contactanos.html" target="_top">Contáctanos</a><br>
                        <a class="texto" href="./pages/servicios.html" target="_top">Servicios</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright">
            <div class="copy">
                ALIGN STYLE &copy; 2022, ALL RIGHT RESERVERED
            </div>
            <div class="info">
                Mas información: 
                <a class="logos-a" href="https://www.facebook.com/ALIGN-STYLE-100589032692689" target="_blank"><i class="fa-brands fa-facebook"></i></a>
                <a class="logos-a" href="https://www.instagram.com/align.style/" target="_blank"><i class="fa-brands fa-instagram"></i></a>
                <a class="logos-a" href="https://wa.me/message/4W4MPALEQJZGE1" target="_blank"><i class="fa-brands fa-whatsapp"></i></a>
                <a class="logos-a" href="https://vm.tiktok.com/ZMNRk35WR/" target="_blank"><i class="fa-brands fa-tiktok"></i></a>
            </div>
        </div>
    </footer>
    <script src="../js/header.js"></script>   
    <script src="https://account.snatchbot.me/script.js"></script><script>window.sntchChat.Init(252214)</script> 

</body>
</html>