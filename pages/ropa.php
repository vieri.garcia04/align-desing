<?php
    require '../modelo/config.php';
    require "../modelo/products.php";
    // session_destroy();
    print_r($_SESSION);

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">

    
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;400&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Anton&family=Kanit:wght@100;400&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cinzel:wght@700;800&family=Montserrat&display=swap" rel="stylesheet">

    <script rel="stylesheet" src="https://kit.fontawesome.com/c174601175.js" crossorigin="anonymous"></script>

    <meta http-equiv=”Expires” content=”0″>
    <meta http-equiv=”Last-Modified” content=”0″>
    <meta http-equiv=”Cache-Control” content=”no-cache, mustrevalidate”>
    <link rel="stylesheet" href="../css/header.css">
    <link rel="stylesheet" type="text/css" href="../css/header.css">
    <link rel="stylesheet" href="../css/footer.css"><link rel="stylesheet" href="../css/index.css">
    <link rel="stylesheet" type="text/css" href="../css/footer.css">
    <link rel="icon" type="image/png" href="../img/logo-alignStyle.png">
    <link rel="stylesheet" href="../css/breadcrumb.css">
    <link rel="stylesheet" href="../css/popover.css">
    <link rel="stylesheet" href="../css/index.css">
    <title>ROPA | ALYGN STYLE</title>
</head>
<body>
    <!-- ********** MENÚ ********** -->
    <header>
        <div class="descuento">
            <h6>ENVIOS GRATUITOS POR COMPRAS MAYORES A S/ 299 SOLES</h6>
        </div>
        <nav class="contenedor_menu">
            <input type="checkbox" id="check">
            <label for="check" class="checkbtn"><i class="fa-solid fa-bars"></i></label>
            <a class="titulo" href="../index.html" target="_top">ALIGN STYLE</a>

            <ul class="ul-header">
                <li><a class="texto" href="../pages/zapatillas.html" target="_top">ZAPATILLAS <i class="fa-solid fa-chevron-down"></i></a></li>
                <li><a  id="ropa-button" class="texto" href="#" target="_top">ROPA <i class="fa-solid fa-chevron-down"></i></a></li>
                <li><a class="texto" href="../pages/accesorios.html" target="_top">ACCESORIOS <i class="fa-solid fa-chevron-down"></i></a></li>
                <li><a class="texto" href="../pages/marcas.html" target="_top">MARCAS <i class="fa-solid fa-chevron-down"></i></a></li>
            </ul>
        
            <div>
                
                <i id="search" class="fa-solid fa-magnifying-glass logos buscar" target="_top"></i>    <!-- Busqueda-->
                <a class="logos login" href="../pages/miCuenta.php" target="_top"><i class="fa-solid fa-user"></i></a> <!--Inicio Sesión -->
                <a class="logos bolsa" href="../checkout.php" target="_top"><i
                        class="fa-solid fa-bag-shopping"></i><span id="num_cart" class="badge bg-secondary contador"><?php echo $num_cart; ?></span></a>
            </div>
        </nav>

        <!-- ********************************  BUSCADOR  *******************************+ -->
        <div class="ctn-bars-search" id="ctn-bars-search" action="">
            <input class="buscar-input" id="input-search" type="text" placeholder="Buscar">
        </div>

        <ul id="box-search">
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Polo</a></li>
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Zapatilla</a></li>
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Pantalon</a></li>
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Camisa</a></li>
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Gorro</a></li>
        </ul>

        <div id="cover-ctn-search"></div>
        <!-- ***************************************************************************** -->
    </header>

    <div id="products-popover" class="popover">
        <ul>
            <li id="1" class="popover-option" >POLOS</li>
            <li id="2" class="popover-option">POLERAS & CASACAS</li>
            <li id="3" class="popover-option">PANTALONES</li>
            <li id="4" class="popover-option">SHORTS</li>
            <li id="ALL" class="popover-option">VER TODO ROPA</li>
        </ul>
    </div>
    <!-- ********** BREADCRUMB ********** -->
    <ul id="breadcrumb" class="breadcrumb">
    </ul>
    <!-- ********** CUERPO DE LA PÁGINA ********** -->

    <div class="contenedor_padre">
        <div class="left">
            <div class="contenedor_filtros">
                <div id="filtro-marcas">
                    <h3>MARCA</h3>
                    <hr>
                    <br>
                </div><br>
                <div id="filtro-talla">
                    <h3>TALLAS DISPONIBLES</h3>
                    <hr>
                    <br>
                </div><br>
                <div id="filtro-sexo">
                    <h3>SEXO</h3>
                    <hr>
                    <br>
                </div>
            </div>
            
        </div>
        <div class="right">
            <div id="products-title" class="titulo_productos">
            </div>
            <div id="products-container" class="contenedor_todos_productos row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3"> 
            
            </div>      
        </div>
        
    </div>
    <script>
        function addProducto(id, token){
            console.log(id, token)
            let url = '../controlador/carrito.php'
            let formData = new FormData()
            formData.append('id', id)
            formData.append('token', token)

            fetch(url, {
                method: 'POST',
                body: formData,
                mode: 'cors'
            }).then(response => response.json())
            .then(data => {
                if(data.ok){
                    let elemento = document.getElementById("num_cart")
                    elemento.innerHTML = data.numero
                }
            })  
        }
    </script>

    <!-- ********** FOOTER ********** -->
    <div class="footer">
        <iframe src="../components/footer.html" width="100%" height="500px"></iframe>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    <script src="../js/header.js"></script>   
    <script src="../js/carrito.js"></script>
    <script src="../js/pedido.js"></script>
    <script src="https://account.snatchbot.me/script.js"></script><script>window.sntchChat.Init(252214)</script> 
    <script src="../js/ropa.js"></script> 
</body>
</html>