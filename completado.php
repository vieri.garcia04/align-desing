<?php
    require 'modelo/config.php';
    require 'modelo/database.php';
    $db = new Database();
    $con = $db->conectar();

    $id_transaccion = isset($_GET['key']) ? $_GET['key'] : '0';

    $error = '';
    if($id_transaccion == ''){
        $error = 'Error al procesar la peticion';
    }else{
        $sql = $con->prepare("SELECT count(id) FROM compra WHERE id_transaccion=? AND status=?");
            $sql->execute([$id_transaccion, 'COMPLETED']);

            if($sql->fetchColumn() > 0){
                $sql = $con->prepare("SELECT id, fecha, email, total FROM compra WHERE id_transaccion=? AND status=? LIMIT 1");
                $sql->execute([$id_transaccion, 'COMPLETED']);
                $row = $sql->fetch(PDO::FETCH_ASSOC);

                $idCompra = $row['id'];
                $total = $row['total'];
                $fecha = $row['fecha'];

                $sqlDet = $con->prepare("SELECT nombre, precio, cantidad FROM detalle_compra WHERE id_compra = ?");
                $sqlDet->execute([$idCompra]);
            }else{
                $error = 'Error al comprar la compra';
            }
    }
    session_destroy();
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <script rel="stylesheet" src="https://kit.fontawesome.com/c174601175.js" crossorigin="anonymous"></script>
    <link href="css/estilos.css" rel="stylesheet">

    <link rel="stylesheet" href="./css/header.css">
    <link rel="stylesheet" type="text/css" href="./css/header.css">
    <link rel="stylesheet" href="./css/footer.css">
    <link rel="stylesheet" type="text/css" href="./css/footer.css">
    <link rel="stylesheet" href="./css/completado.css">
    <link rel="stylesheet" type="text/css" href="./css/completado.css">
    <link rel="stylesheet" href="./css/FontAwesome/all.min.css">
    <link rel="icon" type="image/png" href="./img/logo-alignStyle.png">
    
  </head>
    <title>Tienda</title>
</head>
<body>
    <header>
        <div class="descuento">
            <h6>ENVIOS GRATUITOS POR COMPRAS MAYORES A S/ 299 SOLES</h6>
        </div>
        <nav class="contenedor_menu">
            <input type="checkbox" id="check">
            <label for="check" class="checkbtn"><i class="fa-solid fa-bars"></i></label>
            <a class="titulo" href="./index.php" target="_top">ALIGN STYLE</a>

            <ul class="ul-header">
                <li><a class="texto" href="./pages/zapatillas.html" target="_top">ZAPATILLAS <i
                            class="fa-solid fa-chevron-down"></i></a></li>
                <li><a class="texto" href="./pages/ropa.php" target="_top">ROPA <i
                            class="fa-solid fa-chevron-down"></i></a></li>
                <li><a class="texto" href="./pages/accesorios.html" target="_top">ACCESORIOS <i
                            class="fa-solid fa-chevron-down"></i></a></li>
                <li><a class="texto" href="./pages/marcas.html" target="_top">MARCAS <i
                            class="fa-solid fa-chevron-down"></i></a>
                    <ul class="sub_menu">
                        <li><a href="./pages/adidas.html"><img class="img_prod" src="./img/marcas/adidas.jpeg"
                                    alt=""></a></li>
                        <li><a href=""><img class="img_prod" src="./img/marcas/nb.jpeg" alt=""></a></li>
                        <li><a href=""><img class="img_prod" src="./img/marcas/puma.jpeg" alt=""></a></li>
                        <li><a href=""><img class="img_prod" src="./img/marcas/nike.jpeg" alt=""></a></li>
                        <li><a href=""><img class="img_prod" src="./img/marcas/converse.jpeg" alt=""></a></li>
                        <li><a href=""><img class="img_prod" src="./img/marcas/champion.jpeg" alt=""></a></li>
                        <li><a href=""><img class="img_prod" src="./img/marcas/crocs.jpeg" alt=""></a></li>
                        <li><a href=""><img class="img_prod" src="./img/marcas/kappa.jpeg" alt=""></a></li>
                        <li><a href=""><img class="img_prod" src="./img/marcas/ne.jpeg" alt=""></a></li>
                        <li><a href=""><img class="img_prod" src="./img/marcas/rebels.jpeg" alt=""></a></li>
                    </ul>
                </li>
            </ul>

            <div>

                <i id="search" class="fa-solid fa-magnifying-glass logos buscar" target="_top"></i> <!-- Busqueda-->
                <a class="logos login" href="./pages/miCuenta.php" target="_top"><i
                        class="fa-solid fa-user"></i></a>
                <!--Inicio Sesión -->
                <a class="logos bolsa" href="checkout.php" target="_top"><i
                        class="fa-solid fa-bag-shopping"></i><span id="num_cart" class="badge bg-secondary contador"><?php echo $num_cart; ?></span></a>
                <!--Carrito-->
            </div>
        </nav>

        <!-- ********************************  BUSCADOR  *******************************+ -->
        <div class="ctn-bars-search" id="ctn-bars-search" action="">
            <input class="buscar-input" id="input-search" type="text" placeholder="Buscar">
        </div>

        <ul id="box-search">
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Polo</a></li>
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Zapatilla</a></li>
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Pantalon</a></li>
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Camisa</a></li>
            <li><a href="#"><i class="fa-solid fa-magnifying-glass"></i>Gorro</a></li>
        </ul>

        <div id="cover-ctn-search"></div>
        <!-- ***************************************************************************** -->
    </header>

    <main>
        <div class="container">

        <?php if(strlen($error) > 0){ ?>

            <div class="row">
                <div class="col">
                    <h3><?php echo $error; ?></h3>
                </div>
            </div>

            <?php } else { ?>
            
                <div class="row">
                    <div class="col">
                        <b>Folio de la compra: </b><?php echo $id_transaccion; ?><br>
                        <b>Fecha de compra: </b><?php echo $fecha; ?><br>
                        <b>Total: </b><?php echo MONEDA . number_format($total, 2, '.', ','); ?><br>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Cantidad</th>
                                    <th>Producto</th>
                                    <th>Importe</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php while($row_det = $sqlDet->fetch(PDO::FETCH_ASSOC)){ 
                                    
                                    $importe = $row_det['precio'] * $row_det['cantidad']; ?>
                                    <tr>
                                        <td><?php echo $row_det['cantidad']; ?></td>
                                        <td><?php echo $row_det['nombre']; ?></td>
                                        <td><?php echo $importe; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            <?php } ?>
        </div>
    </main>

                    
    <script src="https://account.snatchbot.me/script.js"></script><script>window.sntchChat.Init(252214)</script> 

</body>
</html>